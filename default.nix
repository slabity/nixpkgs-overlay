self: super:
{
  powerlevel10k = super.callPackage ./powerlevel10k.nix {};

  python3Packages = super.python3Packages.override {
    overrides = self: super: {
      graphene = super.callPackage ./graphene.nix {};
    };
  };

  steam = super.steam.override ({
    extraPkgs = p: with p; [
      libcap.lib
    ];
  });

  preferredRustChannel = super.rustChannelOf {
    date = "2019-12-22";
    channel = "nightly";
  };

  adapta-gtk-theme = super.adapta-gtk-theme.overrideDerivation (oldAttrs: rec {
    configureFlags = [
      "--disable-unity"
      "--disable-gtk_legacy"
      "--enable-gtk_next"

      "--with-selection_color=#CC4349"
      "--with-accent_color=#CC6E13"
      "--with-suggestion_color=#CC8743"
      "--with-destruction_color=#CC8743"
    ];
  });

  wine = super.wine.override ({
    wineRelease = "staging";
    wineBuild = "wineWow";

    pngSupport = true;
    jpegSupport = true;
    tiffSupport = true;
    fontconfigSupport = true;
    tlsSupport = true;
    mpg123Support = true;
    pulseaudioSupport = true;

    openglSupport = true;
    openalSupport = true;
    openclSupport = true;
    vulkanSupport = true;
    sdlSupport = true;
  });

  libvirt = if super.libvirt.version <= "5.4.0" && super.ebtables.version > "2.0.10-4"
  then
  super.libvirt.overrideAttrs (oldAttrs: rec {
    EBTABLES_PATH="${self.ebtables}/bin/ebtables-legacy";
  })
  else super.libvirt;
}
